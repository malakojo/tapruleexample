using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Linq;
using RuleExample;

namespace MyUnitTests
{
    [TestClass]
    public class MyUnitTests
    {
        private IMyInstrument intr;
        [TestMethod]
        public void AllRulesValid()
        {
            intr = Substitute.For<IMyInstrument>();
            var myTestStep = new MyTestStep
            {
                Instrument = intr
            };

            Assert.AreEqual(0, myTestStep.Rules.Count(rule => !rule.IsValid()));
        }
    }
}

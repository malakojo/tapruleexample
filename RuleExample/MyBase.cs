using System;
using System.Linq;
using System.Reflection;
using OpenTap;

namespace RuleExample
{
    [Display("MyTestStep")]
    public abstract class MyBase : TestStep
    {
        protected MyBase()
        {
            //AddInterfaceRules();
        }

        /// <summary>
        /// Adds Rule for every instrument driver that has Display attributes Group containing "Interfaces" and driver implements IInstrument.
        /// </summary>
        private void AddInterfaceRules()
        {
            foreach (var memberInfo in GetType().GetMembers())
            {
                var displayAttr = memberInfo.GetCustomAttribute<DisplayAttribute>(false);
                if (displayAttr == null) continue;

                var interfaceFoundInGroup = displayAttr.Group.Any(node => node.Equals("Interfaces"));

                if (!interfaceFoundInGroup) continue;
                if (memberInfo.MemberType != MemberTypes.Property) continue;

                var propertyInfo = GetType().GetProperty(memberInfo.Name);
                if (propertyInfo is null) continue;
                var value = propertyInfo.GetValue(this);

                if (propertyInfo.PropertyType.GetInterfaces().Any(i => i == typeof(IInstrument)))
                {
                    Rules.Add(() => IsInstrumentNotNull((IInstrument)value), $"Instrument {displayAttr.Name} is not configured in step {Name}", memberInfo.Name);
                }
            }
        }

        protected static bool IsInstrumentNotNull(IInstrument instrument)
        {
            return instrument != null;
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }
    }
}

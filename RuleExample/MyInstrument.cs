using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace RuleExample
{
    public interface IMyInstrument : IInstrument
    {
        void DoSomething();
    }

    [Display("MyInstrument")]
    public class MyInstrument : Instrument, IMyInstrument
    {
        public MyInstrument()
        {
            Name = "MyInstrument";
            // ToDo: Set default values for properties / settings.
        }

        public void DoSomething()
        {
            return;
        }
    }
}

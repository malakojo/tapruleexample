using OpenTap;

namespace RuleExample
{
    [Display("MyTestStep")]
    public class MyTestStep : MyBase
    {
        #region Settings

        [Display("IMyInstrument instance", Group: "Interfaces")]
        public IMyInstrument Instrument { get; set; }

        #endregion
        public MyTestStep()
        {
            Rules.Add(() => IsInstrumentNotNull(Instrument), $"Instrument is not configured in step ", nameof(Instrument));
        }
    }
}
